Title: Response to CIHR surveys, "What are your health research priorities?"
Date: 2019-06-28 15:02
Modified: 2019-06-28 15:02
Category: Science Policy
Tags: canada, sciencepolicy, funding, cihr
Slug: cihr-surveys

My responses to the [summer 2019 CIHR strategic consultation](https://letstalk-cihr.ca/letstalk).

## How can CIHR have the biggest impact?

> CIHR has maintained a focus on four research priorities (i.e., Patient Improvements, Indigenous Health, Prevention, and Chronic Illness) to guide its strategic investments over the last ten years. What advice do you have for CIHR on areas it should be prioritizing in the coming five years?

CIHR should refocus on supporting research excellence, and facilitating the discoveries that can lead to transformative advances in human health. Central planning of research priorities leads us away from what are actually the best research to achieve these transformative changes.

> CIHR’s mandate under the CIHR Act focuses on improving the health of Canadians and the health care system through research. In order to effectively address future and emerging health issues and challenges, what advice do you have for CIHR on a long-term vision for health research over the next thirty years? How can CIHR position Canada to be a leader of research excellence?

I suggest a wholesale refocusing of CIHR around research excellence. Most importantly, the CIHR Project Grant program, the backbone of Canadian health research, should be funded first. A 20-25% success rate is needed to fund all the research proposals judged outstanding by expert reviewers. Funding for bespoke initiatives should come only with funds remaining after that goal is achieved. It’s notable that Canada’s biggest research successes have come from funding systems based on excellence rather than central targeting.

> CIHR strives to demonstrate support for research excellence through leadership in the areas of science policy, including equity, diversity and inclusiveness, ethics, and data management. What advice do you have for CIHR related to our science policies? Are there any areas wherein CIHR should be playing a more significant role? How can CIHR set the standard as a learning research organization?

Canada should be a leader and not a follower when it comes to excellence in science policy.

Equity, diversity, and inclusiveness (EDI): The Dimensions program is a strong start to improving EDI. CIHR should continue supporting this program and should quickly move towards strong financial incentives for institutions to achieve its goals.

Ethics: Handling of ethics complaints needs more transparency. People are reluctant to address misconduct due to little evidence CIHR takes it seriously. Too much investigation is outsourced to institutions, which have a vested interest in sweeping complaints under the rug. We need more independent investigation by federal officials, public notification of particulars where misconduct is deemed to have occurred, and annual reports with summary statistics of the types of complaints received and resolution. Sexual harassment should be treated as research misconduct.

Data management: CIHR should embrace the 15 FAIR Principles for Findable, Accessible, Interoperable, and Reusable data. There should be a presumption that all research data underlying a publication is shared at time of publication. Data described as collected in a progress report must be deposited independently with an accession code. CIHR should publish data management plans for funded grants alongside abstracts in public databases.

Access to research: Full and immediate Open Access to all publications from CIHR-funded research must be required. CIHR should join cOAlition S and endorse Plan S.

Evaluation: CIHR should endorse the San Francisco Declaration of Research Assessment and issue guidance that preprints are evidence of researcher productivity in funding applications and progress reports.

> The CIHR Act specifies that CIHR should pursue international health research partnerships and provide support for the participation of Canadian scientists in such collaborations. What advice do you have for CIHR on balancing funding research in Canada versus supporting Canadian researchers within international research collaborations and partnerships?

Participation in international collaborations can provide a multiplier effect for the resources brought by Canadian researchers. Funding and review of applications should be provided through the Project Grant mechanism rather than creating bespoke funding programs for each potential collaboration. CIHR should guarantee a 20-25% success rate for investigator-initiated applications first, but can provide priority funding for international collaborations rated excellent by experts that are not funded within the investigator-initiated application pool.

> Health research funders consider factors such as the economic or social burden of an illness, the research capacity in the area, the prevalence of a disease, the lack of funding by other organizations, the risk level of the research, and numerous other considerations to help prioritize funding decisions. What advice do you have for CIHR on principles to help guide its decision-making in this regard? How can CIHR be more impactful to promote the health of Canadians?

CIHR must fund Project first, ensuring a 20-25% success rate for investigator-initiated applications. The most important advances in health have come from this sort of broad opportunity-based research in the past and this will continue in the future.

After funding Project first, the most important consideration in targeted funding should be to maximize diversity in research areas and approaches. If CIHR identifies a promising research area that has a dearth of funding through existing mechanisms, it may make sense to provide priority funds for this area as part of the existing Project scheme (thereby minimizing complexity and administrative costs). Other ways of identifying priority funding can often result in providing more funding to areas and laboratories that are already well-funded. Due to diminishing returns this is an inefficient way of spending public funds.

CIHR must seriously examine the impact on equity, diversity, and inclusiveness when considering targeted funding. Targeted funding can exacerbate inequities in who is funded by gender, career stage, and visible minority status. One advantage of redirecting targeted funding as priority funds as part of a larger biannual Project competition is it makes it easier for CIHR to provide equitable equalization of success rates across gender, career stage, and visible minority categories. Success rate equalization should be continued and expanded.

> CIHR often collaborates with other organizations in the design and delivery of its strategic initiatives. What advice do you have for CIHR on the approaches and principles guiding our partnership activities to build a stronger and more connected health research funding ecosystem? How can CIHR be of greater assistance to deliver your organization’s mandate?

CIHR should focus collaborations only with organizations within national governments, both within Canada and in other countries. Strategic initiatives with Canadian non-governmental organizations add unnecessary complexity and provide negative leverage for CIHR funds, skewing research priorities towards politically-connected organizations. The Canadian Common CV should be eliminated and any strategic initiatives should be delivered as priority funding within existing competitions run by one of the organizations involved (for example, CIHR Project Grant, NSERC Discovery Grant, SSHRC Insight Grant). The U.S. National Institutes of Health uses this model for most of its priority funding—priority funding is often provided as a Request for Applications or Program Announcement within the existing R01 scheme. It works well.

> What else would you like to tell CIHR related to its Strategic Priority Areas?

Fund Project first. Commit to 20-25% success rates at Project, then consider funding the other stuff.

## Supporting the next generation of health researchers

> CIHR is responsible under the CIHR Act for attracting, developing and retaining excellent health researchers in Canada. What advice do you have for CIHR to address the development of excellence amongst health research trainees in Canada?

Continue the Canada Graduate Scholarship-Master’s, Canada Graduate Scholarship-Doctoral, and CIHR Fellowship program. Redirect funds from the Vanier Scholarship and Banting Postdoctoral Fellowship to provide more scholarships and fellowships in the above core programs. Stipends for the CIHR Fellowship program should be increased dramatically to make academic research a competitive career option for students and graduates.

> Some health research funders support specialized degrees or programs to enhance the expertise of researchers. What types of programs would you recommend CIHR consider supporting to enhance the skills and expertise of the future generation of health researchers?

Rather than targeting specialized programs, CIHR may wish to consider competitions to provide training grants like the highly successful NIH T32 Institutional Training Grant programs. But if you can’t also provide 20-25% Project success rates it is pointless to add new training programs that will lead to a cliff ending the trainees’ careers. It is important to remember that most Project operating funds go to paying for trainees and their training.

> CIHR’s support for capacity development outside of the Tri-Council programs has focused a large proportion of the budget on awards and fellowships for graduate students and post-doctoral fellows. What advice do you have for CIHR in the area of capacity development to ensure health researchers are effectively supported across their entire careers?

Capacity development after the training phase of a career is best addressed by ensuring adequate operating funding. CIHR Project success rates must go up, to 20-25% or more.

> CIHR currently makes efforts to increase equitable access to research funding for groups in Canada (e.g., early career investigators, Indigenous health researchers). What groups, if any, would benefit from further attention from CIHR? In addition, what research areas or types of research would benefit from CIHR investment to build capacity?

A focus on ensuring equitable access by gender must continue. CIHR should also ensure equitable access to research funding for visible minorities and for other career stages such as mid-career investigators. Building capacity through the first five years of an independent researcher’s career and then throwing them off a cliff is a failure for CIHR’s capacity-building remit.

> What else would you like to tell CIHR related to its Research Capacity Development mandate?

Assurance of equitable operating funding is essential for developing—and retaining—research capacity. This must be a priority and strategies for research capacity must integrative approach that includes ensuring sufficient and equitable operating funding.

Without 20-25% success rates in open operating grants, CIHR will fail at its research capacity development mandate. The horse needs to be placed before the cart.

## Translating research evidence into practice

> CIHR’s mandate from the CIHR Act includes a clear responsibility to focus on both knowledge creation and knowledge translation. What advice do you have for CIHR to ensure it has the right balance of knowledge creation and knowledge translation funding? What funding programs, policies or activities should CIHR pursue in the area of knowledge translation? How can CIHR be more impactful to promote the health of Canadians?

CIHR is already a leader in this area and the balance of funding is already appropriate.

There should be an emphasis not just on researcher-to-health care provider knowledge translation but also researcher-to-researcher knowledge translation. Much knowledge gained that is lost when the only way of communicating it is via publication. CIHR should require resource sharing plans from all grant applicants that include knowledge translation, and evaluate applicants on their track record of sharing resources, protocols, constructs, code, and data. It should also make resource sharing plans public so they are self-enforcing.

Full and immediate Open Access to all publications from CIHR-funded research must be required. CIHR should join cOAlition S and endorse Plan S.

> CIHR currently invests operational resources to support programs that facilitate a relationship between knowledge users and researchers and provide a shared space for dialogue. What types of programs or activities should CIHR support to further engage knowledge users, including Canadian citizens, policy-makers and clinicians?

Current programs are acceptable.

> What else would you like to tell CIHR related to its Knowledge Translation mandate?

The public engagement of CIHR itself overemphasizes publicizing funding rather than research results. The research results are the point of all of CIHR’s activity. CIHR’s public engagement and media relations team needs a reorientation towards publicizing CIHR-funded research after it is completed, rather than the funding before it starts. And needs to ensure that CIHR gets proper credit for this research.

Requiring Canadian Common CV creation by knowledge users is incredibly counterproductive and must be ended.

## How should CIHR spend its money?

> Within its discretionary budget, CIHR has historically directed a large majority of its funds to Investigator Initiated Research based on the rationale that this will generate innovative and timely discoveries, practices and policies. What advice would you give CIHR on an appropriate balance between funding ideas generated within the research community versus funding priorities identified by governments, partners, Institutes and CIHR?

The core of health research in Canada is the investigator-initiated Project grant. CIHR needs to fund Project first and commit to success rates of 20-25% without across-the-board budget cuts. Any less causes the health research system to break down as many outstanding projects remain unfunded. Funding priorities identified by others are a “nice to have” that should be paid for with remaining funds after Project.

> CIHR has consistently funded research platforms in Canada to support excellence in health research. What advice do you have for CIHR on support for national platforms that enable the health research enterprise?

As the Fundamental Science Review recommends, there should be a government-wide National Advisory Council on Research and Innovation to rationalize funding of central resources.

> The allocation of the same $8.6M annual grant budget for each Institute has been in place for more than a decade. What advice do you have for CIHR on the allocation of the grant budgets to CIHR Institutes?

Institute budgets should not be equal. They should have a component that is proportional to the amount of investigator-initiated research funded that aligns to that institute. Also, no institute budget should be increased until Project success rates are restored to 20-25%. Fund Project first. Don’t lose sight of what’s most important.

> What else would you like to tell CIHR related to the Budget?

Fund Project first. The first priority absolutely has to be restoring success rates of 20-25% in Project because the health research system breaks down without that. Investment is lost and unsound decisions are made. Any other targeted funding or strategic initiatives should be made with funds left over after fulfilling this commitment.

To the extent possible, any targeted funding should be allocated through priority funding of applications submitted and evaluated through the existing Project competitions. This will save money, reduce administrative waste and review burden, and ensure that the applications are judged most fairly. Setting up a new competition for every instance of targeted funding is wasteful.

## How CIHR is organized: The 13 Institutes

> The existing slate of Institutes, and their mandate areas, were developed during the creation of CIHR in 2000. What advice do you have for CIHR on the slate of Institutes, and corresponding mandate areas, to ensure they effectively addresses all major areas of health research? If CIHR were to be created today, for example, what slate of institutes would you see as essential for CIHR to meet its mandate?

CIHR is missing an institute of general medical sciences. The NIH’s National Institute of General Medical Sciences has for many years funded some of the most impactful research in the world, leading to significant advances in science, health, and multiple Nobel prizes. This would be a good model to follow.

> The Institutes are responsible for a variety of activities including identifying strategic priorities and gaps in their mandate areas, designing and implementing strategic funding opportunities, building capacity within particular areas of research, and supporting researchers, groups, and communities, all in collaboration with key partners. What advice do you have for CIHR on the activities and responsibilities of CIHR Institutes?

All Open grants should be assigned to one institute or another and the institute should have the responsibility of some post-award aspects of managing these grants. The current institute structure results in an overemphasis on a minority of CIHR’s financial activity, and a misalignment of incentives and goals between CIHR Science Council and the broader community of researchers. While there should be no institute involvement before the peer review committee, CIHR should assign each Open grant to one institute’s portfolio based on the applicant’s selection. Any success metrics for CIHR activity should be broken down by the institute it is associated with, and the appropriate institutes should be credited in any funding acknowledgments in papers and in press releases.

Institutes should get out of the business of operating grant competitions and organizing their review, to the extent possible. Instead, targeted funds should be allocated through an expansion of the current priority announcement system beyond its use for bridge grants. This should be for funding priority areas in addition to and beyond the base 20-25% success rate expected for investigator-initiated proposals.

> What else would you like to tell CIHR related to the Institutes?

The duplication of effort involved in creating separate newsletters for each institute should end.

## CIHR’s largest funding program: The Project Grant competition

> The Project Grant Competition does not have pre-defined limits on the duration of a grant or the budget size of a grant. This has historically been in place to ensure that all types of health research can effectively be supported by the program. What advice do you have for CIHR on grant duration or grant budgets in the Project Grant Competition in order to effectively balance funding all types of research grants with supporting an appropriate number of health researchers?

Across-the-board cuts should be ended. They distort the evaluation of budgets.

Generally, 5 year grants provide a nice trade-off between giving researchers flexibility to conduct research efficiently and ensuring what we are funding is the best research. Grant periods of more than 5 years should be rare and require explicit justification and review. In my experience as a reviewer, the current panel system works well for grants that should be of less than 5 years in duration.

> The Project Grant Competition peer-review process relies on panels that review proposals in 47 areas of science. What advice do you have for CIHR on the current peer-review process and complement of panels? Are there areas of science or types of research that you feel are not appropriately reviewed within the current complement of panels?

I have no specific comments on the current makeup of peer review panels, but there should be much more transparency in how the decisions about peer review composition and mandate are made, and someone specific should be accountable for these decisions. It is not at all clear who makes these decisions now and why. It is under-appreciated how the makeup of the slate of panels defines what research is done, especially for research which might represent an edge case between the expertise and interests of multiple panels.

The composition of peer review panels should be examined routinely (such as annually), not only in the case of a strategic consultation such as this one.

> CIHR is dedicated to ensuring there are no systematic biases within its review processes while maintaining a focus on scientific excellence. What advice do you have for CIHR related to the goal of upholding equity and fairness in the review system?

CIHR should commit to success rate equalization as a way to ensure equity. This works well for early-career investigators and by gender and should be expanded to other categories in which equitable results are desired, such as visible minorities in Canada. CIHR should seek 20-25% success rates among all of these groups. Success rate equalization is the only thing that will address structural biases against some groups of applicants—you cannot do it with bias training alone. Rolling targeted funding into the Project competition instead of having separate competitions will eliminate the difficulties of equalizing small competitions.

The false dichotomy raised between “excellence” and equitable treatment of researchers who have had to apply extra effort to overcome obstacles is incredibly insulting and must be ended. Often decisions are being made between grant applications that are all scored in the outstanding range and have high variance among reviewer scores. In this context, it is statistically innumerate to claim that an application is somehow more “excellent” than another with a score that is slightly lower but still well within the scoring variance.

CIHR should also take its responsibilities under the CIHR Act to “attract, develop and keep excellent researchers” more seriously. This means ensuring success rate equalization for mid-career investigators as well. It is wasteful to invest money in training researchers, setting up new research programs, and then cutting them off at their knees as the research gains full steam. Arguments that attrition of mid-career investigators is desirable are specious and based on experiences two decades ago when success rates were in the 30-40% range.

> What else would you like to tell CIHR related to the Investigator Initiated Research Program and review processes?

Resource sharing plans should be a mandatory part of applications and should be made public for awarded grants so they are self-enforcing.

Having three-year peer review committee appointments is good but also need to ensure rotation of people on and off the committee.

The Canadian Common CV is an incredible waste of time for applicants, worthless for reviewers, and a waste of public funds to maintain in the face of much simpler alternatives. Please replace it with the NIH biosketch.

## Acknowledgments

Thanks to Max Rousseaux and Holly Witteman for helpful comments.

My responses were influenced by the well-thought-out responses of [Michael Hendricks](https://medium.com/@MHendr1cks/cihr-consultation-responses-7412608ad0b8) and [Jim Woodgett](https://medium.com/@jwoodgett/lets-talk-cihr-part-4-1df8430d1e31).
